from time import sleep, time
from listpage_crawler import ptt_listpage_crawler
import pandas as pd


def get_ranking(day_bias=7):
    '''
        Input day_bias: 要查詢的天數(正整數)
        return msg_r : 可傳給使用者看的查詢結果(string)
        return df_r : 查詢結果組成的sorted DataFrame
    '''
    # PTT表特版首頁
    url = "https://www.ptt.cc/bbs/Beauty/index.html"

    day_bias = -day_bias + 1

    start_time = time()
    post_list = []
    delay_sec = 0.1
    count = 0
    if day_bias > 0:
        print("Check day_bias!!!")
    # while url != None:
    while url is not None:
        url, next_post_list = ptt_listpage_crawler(url, day_bias)
        post_list = post_list + next_post_list[::-1]
        count += 1
        sleep(delay_sec)
    end_time = time()
    duration = end_time - start_time

    # 將push轉成int
    # 處理'爆'或'噓' --> 改成99或-1
    for p in post_list:
        try:
            p['push'] = int(p['push'])
        except ValueError:
            if '爆' == p['push']:
                p['push'] = int('99')
            else:
                p['push'] = int('-1')

    # Print for debugging
    for p in post_list:
        print("{}\t推：{}\t{}  {}" .format(
                                          p['date'],
                                          p['push'],
                                          p['title'],
                                          p['post_url']))

    print("-------------------------- Summary --------------------------")
    print(" 天數：{}天\t頁數：{}頁\t筆數：{}\t耗時：{:4.2f}秒" .format(1+abs(day_bias), count, len(post_list), duration))
    print("-------------------------- Summary --------------------------")

    # 依照指定天數爬文，所得結果製作成DataFrame (未排序)
    col_index = ['date', 'push', 'title', 'post_url']
    df_all = pd.DataFrame(columns=col_index)
    for p in post_list:
        s = pd.Series([p['date'],
                       p['push'],
                       p['title'],
                       p['post_url']],
                       index=col_index)
        df_all = df_all.append(s, ignore_index=True)

    # 排序DataFrame
    df_sorted = df_all.sort_values(by='push', ascending=False)

    # 只保留前10筆，並重新編列index
    if len(post_list) < 10:
        df_sorted = df_sorted.head(len(post_list))
        df_sorted.index = range(1, len(post_list)+1)
    else:
        df_sorted = df_sorted.head(10)
        df_sorted.index = range(1, 11)

    # 新增欄位'id_name'存放網址中的唯一字串
    df_sorted['id_name'] = df_sorted['post_url'].str[30:].str[:-5]
    df_sorted['id_name'] = df_sorted['id_name'].str.replace('.', '_')

    # print(df_sorted)

    # export to csv
    # df_sorted.to_csv("top10.csv", encoding='utf-8', index=False)

    # 製作回應資訊(reply message)
    msg = ""
    for id in range(len(df_sorted)):
        (push, title) = df_sorted.iloc[id][['push', 'title']]
        msg = msg + "💎[Top " +\
              (str(id+1) if id >= 9 else (" "+str(id+1))) + "][推" +\
              ('爆' if push == 99 else str(push))+"]" +\
              title[:5] + "\n    " + title[5:] +\
              ("" if id == len(df_sorted)-1 else "\n\n")

    # print(msg)
    return msg, df_sorted
