import os
from flask import Flask, request, abort

from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage, ImageSendMessage, FollowEvent,
    TemplateSendMessage, ButtonsTemplate, MessageAction, URIAction,
    PostbackAction, PostbackEvent, QuickReplyButton, LocationAction,
    ImageMessage, VideoMessage, TemplateSendMessage, ConfirmTemplate
)
from richmenu_handling import (richmenu_op_delete, richmenu_op_get_list,
                               richmenu_op_link, richmenu_op_unlink
                              )
from days_ranking import get_ranking
from postpage_crawler import post_crawler, rm_img, img_dl_zip
from s3_remote import s3_sharing_flow
from time import time


app = Flask(__name__)

line_bot_api = LineBotApi(
    'your_key')
handler = WebhookHandler('key')


# 建立rich menu id list
rm_tmpl_id_list = richmenu_op_get_list()
rm_local_id_list = {'RM_get_zip': None, 'RM_img_last': None, 'RM_img_next5': None}

# global variables
post_id = ""
result_msg = ""
ranking_msg = ""
result_RM_name = ""
result_df = None
img_name_list = []
img_url_list = []
img_url_list_duplicate = []
img_remain = 0


@app.route("/callback", methods=['POST'])
def callback():
    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']
    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)

    print(body)  # 會看到Event ... Ex: 封鎖、解封、關注

    # handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        print("Invalid signature. Please check your channel access token/channel secret.")
        abort(400)
    return 'OK'


@handler.add(FollowEvent)
def handle_follow(event):
    # 取得 user_id
    user_id = line_bot_api.get_profile(event.source.user_id).user_id
    print("user_id is", user_id)

    # 建立文字消息，並請 line_bot_api 傳送「Greeting message」
    reply_text_message = TextSendMessage("感謝你的關注！\n請直接點選選單內容操作～")
    line_bot_api.reply_message(event.reply_token, reply_text_message)

    # 查 rich menu id
    next_rm_id = rm_tmpl_id_list['RM_main']
    print("next rm id is:", next_rm_id)

    # Link to rich menu
    print("Link user {} to menu {}" .format(user_id, next_rm_id))
    richmenu_op_link(user_id, next_rm_id)


@handler.add(MessageEvent, message=TextMessage)
def handle_text_message(event):
    msg_in = event.message.text
    if msg_in == '下載所有圖片':
        reply_text_message = TextSendMessage("已開始為您打包成zip!\n請稍等下載連結...")
        line_bot_api.reply_message(event.reply_token, reply_text_message)
    elif msg_in == 'XXX':
        pass
    else:
        reply_text_message = TextSendMessage("請直接點選選單內容～～")
        line_bot_api.reply_message(event.reply_token, reply_text_message)


'''
告知handler若收到PostbackEvent時要做什麼
    判斷postback的data欄位
'''
@handler.add(PostbackEvent)
def handle_postback_event(event):
    global post_id
    global result_msg
    global ranking_msg
    global result_RM_name
    global result_df
    global img_name_list, img_url_list, img_url_list_duplicate
    global img_remain

    if 'PB_day' in event.postback.data:
        # 取得 user_id
        user_id = line_bot_api.get_profile(event.source.user_id).user_id
        print("user_id is", user_id)

        # 取得天數
        days = int(event.postback.data[6:])

        # 爬 days 天
        (ranking_msg, result_df) = get_ranking(days)
        print(ranking_msg)


        # 根據DataFrame長度決定要發出top多少的Rich menu
        num_rank = len(result_df)
        if num_rank < 3:
            print("num_rank < 3 !!!")  # 再加上一點處理
            next_rm_id = rm_tmpl_id_list['RM_main']  # 不變
            reply_text_message = TextSendMessage("今日文章目前累積還不到3篇，建議選3、5、7天來看喔！")
            line_bot_api.reply_message(event.reply_token, reply_text_message)
        elif 3 < num_rank < 11:
            result_RM_name = "RM_top" + str(num_rank)
            next_rm_id = rm_tmpl_id_list[result_RM_name]  # 查menu id
            # 回覆爬文結果
            reply_text_message = TextSendMessage(ranking_msg)
            line_bot_api.reply_message(event.reply_token, reply_text_message)
        else:
            print("[Error] RM index out of range!")

        print("next rm id is:", next_rm_id)

        # save result
        result_msg = ranking_msg
        result_RM_name = result_RM_name

        # Link to rich menu
        print("Link user {} to menu {}".format(user_id, next_rm_id))
        richmenu_op_link(user_id, next_rm_id)

    elif event.postback.data == 'PB_go_result':
        # 取得 user_id
        user_id = line_bot_api.get_profile(event.source.user_id).user_id
        print("user_id is", user_id)

        # empty download list
        img_url_list_duplicate = []

        # 回覆爬文結果
        reply_text_message = TextSendMessage(ranking_msg)
        line_bot_api.reply_message(event.reply_token, reply_text_message)

        top = len(result_df) if len(result_df) >= 3 else 3
        result_RM_name = "RM_top" + str(top)
        next_rm_id = rm_tmpl_id_list[result_RM_name]  # 查menu id
        print("next rm id is:", next_rm_id)

        # Link to rich menu
        print("Link user {} to menu {}".format(user_id, next_rm_id))
        richmenu_op_link(user_id, next_rm_id)

    elif event.postback.data == 'PB_go_home':
        # 取得 user_id
        user_id = line_bot_api.get_profile(event.source.user_id).user_id
        print("user_id is", user_id)

        # reply_text_message = TextSendMessage("Got PB_go_home")
        # line_bot_api.reply_message(event.reply_token, reply_text_message)

        # empty download list
        img_url_list_duplicate = []

        # 查 rich menu id
        next_rm_id = rm_tmpl_id_list['RM_main']
        print("next rm id is:", next_rm_id)

        # Link to rich menu
        print("Link user {} to menu {}".format(user_id, next_rm_id))
        richmenu_op_link(user_id, next_rm_id)

    elif event.postback.data == 'PB_img_next5':
        # 取得 user_id
        user_id = line_bot_api.get_profile(event.source.user_id).user_id
        print("user_id is", user_id)

        # 準備要發送的下5張圖片
        image_message_list = []
        for _ in range(5):
            img_pop = img_url_list.pop(0)
            m = ImageSendMessage(
                original_content_url=img_pop,
                preview_image_url=(img_pop[:-4] + 'm' + img_pop[-4:])
            )
            image_message_list.append(m)
            img_remain -= 1  # 發出 preview img
        # 發送圖片訊息
        line_bot_api.reply_message(event.reply_token, image_message_list)

        print("[PB_ing_next5] 發送圖片完成！ 剩餘張數: ", img_remain)

        # 根據剩餘的照片張數決定 next rich menu，查menu id
        if 0 < img_remain <= 5:
            next_rm_id = rm_tmpl_id_list['RM_img_last']
        else:  # img_remain > 5
            next_rm_id = rm_tmpl_id_list['RM_img_next5']
        print("next rm id is:", next_rm_id)

        # Link to rich menu
        print("Link user {} to menu {}".format(user_id, next_rm_id))
        richmenu_op_link(user_id, next_rm_id)

    elif event.postback.data == 'PB_img_last':
        # 取得 user_id
        user_id = line_bot_api.get_profile(event.source.user_id).user_id
        print("user_id is", user_id)

        # 準備要發送的剩餘圖片
        image_message_list = []
        for _ in range(img_remain):
            img_pop = img_url_list.pop(0)
            m = ImageSendMessage(
                original_content_url=img_pop,
                preview_image_url=(img_pop[:-4] + 'm' + img_pop[-4:])
            )
            image_message_list.append(m)
            img_remain -= 1  # 發出 preview img
        # 發送圖片訊息
        line_bot_api.reply_message(event.reply_token, image_message_list)

        print("[PB_ing_last] 發送圖片完成！ 剩餘張數: ", img_remain)

        next_rm_id = rm_tmpl_id_list['RM_get_zip']
        print("next rm id is:", next_rm_id)

        # Link to rich menu
        print("Link user {} to menu {}".format(user_id, next_rm_id))
        richmenu_op_link(user_id, next_rm_id)

    elif event.postback.data == 'PB_get_zip':
        user_id = line_bot_api.get_profile(event.source.user_id).user_id

        # 開始下載圖片、打包、刪圖
        zipfile_name = img_dl_zip(img_url_list_duplicate, post_id)
        img_url_list_duplicate = []   # terminate

        # 上傳、生成 pre-signed url、刪除本地壓縮檔
        pre_url = s3_sharing_flow(zipfile_name,  # 本地檔名
                                  "ptt-linebot",  # Bucket name
                                  "result_ZIP/")  # S3存放路徑與檔名
        print("pre-signed url: ", pre_url)

        try:
            # 發送下載確認訊息
            confirm_template_message = TemplateSendMessage(
                alt_text='Confirm template',
                template=ConfirmTemplate(
                    text=' 打包所有圖片 ---> 下載zip檔案 ',
                    actions=[
                        URIAction(
                            label='下載',
                            uri=pre_url
                        ),
                        PostbackAction(
                            label='回結果',
                            data='PB_go_result'
                        )
                    ]
                )
            )
            line_bot_api.reply_message(event.reply_token, confirm_template_message)
        except:
            reply_error_msg = TextSendMessage("oops! 出了點狀況！\n可能是這篇照片數量太多...\n只好您建議單張下載了～")
            line_bot_api.reply_message(event.reply_token, reply_error_msg)

        next_rm_id = rm_tmpl_id_list['RM_result_home']
        # Link to rich menu
        print("Link user {} to menu {}".format(user_id, next_rm_id))
        richmenu_op_link(user_id, next_rm_id)

    elif 'PB_top' in event.postback.data:
        start_time = time()
        # 取得 user_id
        user_id = line_bot_api.get_profile(event.source.user_id).user_id
        print("user_id is", user_id)
        # reply_text_message = TextSendMessage("Got PB_top" + event.postback.data[6:])

        # 取出數值
        top = int(event.postback.data.replace("PB_top", ""))
        print("top number is:", top)
        # print(result_msg)
        # print(result_RM_name)

        # 根據回應取得post url
        post_url = result_df.at[top, 'post_url']
        post_id = result_df.at[top, 'id_name']

        # post crawler (post_id傳入後會成為zip檔名)
        # 爬post並回傳:(回應使用者的文字資訊, 圖片檔名的list)
        reply_msg, img_name_list, img_url_list = post_crawler(post_url, post_id)
        # img_url_list_duplicate = img_url_list
        for l in img_url_list:
            img_url_list_duplicate.append(l)

        img_remain = len(img_url_list)
        print("--------- Reply Msg ---------")
        reply_crawl_result = TextSendMessage(reply_msg + "\n照片張數: " + str(img_remain))
        print(reply_crawl_result)
        print("-----------------------------")

        # for u in img_url_list:
        #     print(u)

        # 檢查preview image是否格式正確（若連結為無副檔名則加上）
        # preview_img = img_url_list[0]
        preview_img = img_url_list.pop(0)
        if preview_img[-4:] != ".png" and preview_img[-4:] != ".gif" and preview_img[-3:] != "jpg":
            preview_img += ".jpg"

        print("preview image: ", preview_img)
        # 傳送圖片
        image_message = ImageSendMessage(
                                         original_content_url=preview_img,
                                         preview_image_url=(preview_img[:-4] + 'm' + preview_img[-4:])
        )
        img_remain -= 1  # 發出 preview img

        try:
            line_bot_api.reply_message(event.reply_token, [reply_crawl_result, image_message])
        except:
            line_bot_api.reply_message(event.reply_token, [reply_text_message, reply_crawl_result])

        # 根據剩餘的照片張數決定 next rich menu，查menu id
        if img_remain == 0:
            next_rm_id = rm_tmpl_id_list['RM_result_home']
        elif 0 < img_remain <= 5:
            next_rm_id = rm_tmpl_id_list['RM_img_last']
        else:  # img_remain > 5
            next_rm_id = rm_tmpl_id_list['RM_img_next5']
        print("next rm id is:", next_rm_id)

        # Link to rich menu
        print("Link user {} to menu {}".format(user_id, next_rm_id))
        richmenu_op_link(user_id, next_rm_id)

        end_time = time()
        print("In postback handler #1 ...... {} to {}. Duration: {}"
              .format(start_time, end_time, end_time-start_time))

# for local testing with ./ngrok
# if __name__ == "__main__":
#     app.run(host="0.0.0.0")

# heroku 專用，偵測keroku給我們的port
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=os.environ['PORT'])
